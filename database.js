const mongoose = require('mongoose');
const dbConfig = require('./config/dbConfig')


const connection = mongoose.connect(dbConfig.CONNECTION_STRING, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })
    .then(() => {
        console.log("connected to database: "+ dbConfig.environment);
    })
    .catch((error) => {
        console.log('Connection failed: ' + error);
    });

module.exports = connection;