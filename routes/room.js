const express = require("express");
const router = express.Router();

const RoomService = require('../services/room.service');

const checkAuth = require("./../middleware/check-auth");
/**
 * get all rooms
 */
router.get("", async (req, res, next) => {

    let result = await RoomService.getAll();

    if (result.rooms.length > 0) {
        res.status(200).json({
            message: 'Rooms fetched successfully',
            rooms: result.rooms
        });
    } else {
        res.status(404).json({
            message: 'No Rooms Found',
            rooms: documents
        });
    }
});

/**
 * get all rooms by company Id
 */
router.get("/:company_id", async (req, res, next) => {
    const company_id = req.params.company_id;
    let result = await RoomService.getByCompanyId(company_id);

    if (result.rooms.length > 0) {
        res.status(200).json({
            message: 'Rooms fetched successfully',
            rooms: result.rooms
        });
    } else {
        res.status(404).json({
            message: 'No Rooms Found',
            rooms: result.rooms
        });
    }
});


/**
 * save a room
 */
router.post("/:name", async (req, res, next) => {
    const company_name = req.params.name;

    const roomDTO = {
        name: req.body.name,
        maxCapacity: req.body.maxCapacity,
        hasProjector: req.body.hasProjector,
        hasTelephone: req.body.hasTelephone
    }

    let result = await RoomService.save(company_name, roomDTO);
    if (result.status) {
        res.status(200).json({
            status: true,
            room: result.roomRes
        });
    } else {
        res.status(404).json({
            status: false,
            message: result.message
        });
    }
});



/**
 * test route
 */
router.post("/test/:name", async (req, res, next) => {
    const company_name = req.params.name;

    const bookingRequest = {
        date: req.body.date,
        slot: {
            startTime: req.body.slot.startTime,
            endTime: req.body.slot.endTime
        },
        capacity: req.body.capacity,
        needsTelephone: req.body.needsTelephone,
        needsProjector: req.body.needsProjector,

    }

    let result = await RoomService.getByBooking(company_name, bookingRequest);
    if (result.status) {
        res.status(200).json({
            status: true,
            rooms: result.rooms
        });
    } else {
        res.status(404).json({
            status: false,
            message: "Rooms Not Found."
        });
    }

});


module.exports = router;