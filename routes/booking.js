const express = require("express");
const router = express.Router();

const BookingService = require('../services/booking.service');
const checkAuth = require("./../middleware/check-auth");
/**
 * book a room
 */
router.post("/book/:name",checkAuth, async (req, res, next) => {
    const company_name = req.params.name;
    console.log(req.body);
    
    const bookingRequest = {
        date: req.body.date,
        slot: {
            startTime: req.body.slot.startTime,
            endTime: req.body.slot.endTime
        },
        capacity: req.body.capacity,
        needsTelephone: req.body.needsTelephone,
        needsProjector: req.body.needsProjector
    }

    let result = await BookingService.add(company_name, bookingRequest);
    if (result.status) {
        res.status(200).json({
            status: true,
            message: "Room Booked Successfully",
            booking: result.booking,
            room:result.room
        });
    } else {
        res.status(200).json({
            status: false,
            message: "Room could Not be Booked",
            message: result.message
        });
    }

});


/**
 * get bookings by room id
 */
router.get("/book/:id", async (req, res, next) => {
    const roomId = req.params.id;

    let result = await BookingService.getBookingsByRoom(roomId);
    if (result.bookings.length > 0) {
        res.status(200).json({
            status: true,
            message: "Bookings found Successfully",
            booking: result.bookings
        });
    } else {
        res.status(200).json({
            status: false,
            message: "No Bookings found",
            message: result.message
        });
    }

});

module.exports = router;